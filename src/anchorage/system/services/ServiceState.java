package anchorage.system.services;

import java.io.Serializable;

public enum ServiceState
  implements Serializable
{
  OFFLINE,  ONLINE;
  
  private ServiceState() {}
}


/* Location:           C:\FTPSite\FxMozart-API\abos-dist-2.3.5.8.12-fxmozart-api.jar
 * Qualified Name:     anchorage.system.services.ServiceState
 * JD-Core Version:    0.7.0.1
 */