package anchorage.timedservices;

import javax.ejb.Remote;

@Remote
public abstract interface ITimedServiceBean
{
  public abstract void process();
}


/* Location:           C:\FTPSite\FxMozart-API\abos-dist-2.3.5.8.12-fxmozart-api.jar
 * Qualified Name:     anchorage.timedservices.ITimedServiceBean
 * JD-Core Version:    0.7.0.1
 */