
package baxterapitest;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import anchorage.serverproxy.services.ClientServiceDispatcher;
import anchorage.services.IPositionSummaryManager;
import anchorage.services.report.ICSVReportService;
import anchorage.services.retail.IBonusManagerService;
import anchorage.services.reval.CcyPosition;
import anchorage.systemconfig.IFxSpreadService;
import anchorage.trade.ICommonTrade;
import anchorage.trade.SpotTrade;
import anchorage.utils.Formatter;
import java.util.Iterator;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
 
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
/**
 *
 * @author Administrator
 */
public class BaxterAPITest {

    private static String config = "connection.properties";
    
     static Logger myLogger = Logger.getLogger(BaxterAPITest.class.getName());  
    static Appender myAppender;  
    public static void main(String[] args) throws IOException {
       //get PositionSummaryManager
		
         
        myLogger.setLevel(Level.ALL);  
          
        // Define Appender     
        myAppender = new ConsoleAppender(new SimpleLayout());  
  
        //myAppender.setLayout(new SimpleLayout());  
        myLogger.addAppender(myAppender);
        //load config
		InputStream stream = BaxterAPITest.class.getResourceAsStream(config);
                
                Properties clientProperties = new Properties();
		clientProperties = new Properties();
		clientProperties.load(stream);
                
                if(stream==null)
                {
                  myLogger.info("Couldn't get the stream from resource?");
                }
                
                if(clientProperties==null)
                {
                  myLogger.info("Couldn't get the properties from resource?");
                }
                Logger.getLogger(BaxterAPITest.class.getName()).addAppender(myAppender);
     //   anchorage.serverproxy.services.ClientServiceDispatcher.getLogger().addAppender(myAppender);
        
                IPositionSummaryManager positionSummaryManager = ClientServiceDispatcher.getPositionSummaryManager();
		 //login
		positionSummaryManager.loginToContext(clientProperties);

		
		System.out.println("Account id loaded:"+positionSummaryManager.getAccountId() + " current equity:"+positionSummaryManager.getNetEquity());
                 Calendar sessionStartCalendar = Calendar.getInstance();
		sessionStartCalendar.set(Calendar.YEAR, 2013);
		sessionStartCalendar.set(Calendar.MONTH, 9);
		sessionStartCalendar.set(Calendar.DAY_OF_MONTH, 1);
		System.out.println(Formatter.formatDate(sessionStartCalendar));

		Calendar sessionEndCalendar = Calendar.getInstance();
		System.out.println(Formatter.formatDate(sessionEndCalendar));
                List<ICommonTrade> trades=  positionSummaryManager.getAllTrades(sessionEndCalendar, sessionEndCalendar);
                for(ICommonTrade trade :trades)
                { 
                    
                    SpotXmlTrades tr =new SpotXmlTrades(trade);
                     Element dc = tr.ToXml();
                     StringBuilder bl = new StringBuilder();
                     myLogger.info("Spot Trade:"+ bl.append(dc.getTextContent()));
                     
                }
                
                
                //open trades
                
        List<ICommonTrade> openTrades = positionSummaryManager.getOpenTrades();
        for (Iterator<ICommonTrade> it = openTrades.iterator(); it.hasNext();) {
            SpotXmlTrades trade =  new SpotXmlTrades(it.next());
             Element dc = trade.ToXml();
            StringBuilder bl = new StringBuilder();
            myLogger.info("Spot Trade:"+ bl.append(dc.getTextContent()));
            trade.TradesToFile("trade.xml");        
                  
        }
                
                
    }
    
    
    
    public void GetTrades(IPositionSummaryManager manager)
    {
        manager.updatePortfolio();
    }
}
