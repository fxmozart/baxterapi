/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package baxterapitest;

import anchorage.trade.ICommonTrade;
import anchorage.trade.SpotTrade;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Name;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author Administrator
 */
public class SpotXmlTrades extends anchorage.trade.SpotTrade implements ICommonTrade
{

    org.w3c.dom.Document dom;
    public SpotXmlTrades(ICommonTrade tr)
    {
        this.setAccountId(tr.getAccountId());
        this.setAckMask(tr.getAckMask());
        this.setAggregateAccountId(tr.getAggregateAccountId());
        this.setAmtCcy1(tr.getAmtCcy1());
        this.setAmtCcy1EUR(tr.getAmtCcy1EUR());
        this.setAmtCcy1USD(tr.getAmtCcy1USD());
        this.setCcy1Code(tr.getCcy1Code());
        this.setCcy2Code(tr.getCcy2Code());
        this.setClosingDate(tr.getClosingDate()==null?0:tr.getClosingDate());
        this.setClosingProfitOrLoss(tr.getClosingProfitOrLoss());
        this.setCounterPartyAccountId(tr.getCounterPartyAccountId());
        this.setCreatedByEodRunId(tr.getCreatedByEodRunId());
        this.setDealtRate(tr.getDealtRate());
        this.setEffectiveRate(tr.getEffectiveRate());
        this.setExecutionTime(tr.getExecutionTime());
        this.setExtAccountRef(tr.getExtAccountRef());
        this.setExtMarketMakerCode(tr.getExtMarketMakerCode());
        this.setExtOrderRef(tr.getExtOrderRef());
        this.setExtSourceRef(tr.getExtSourceRef());
        this.setExtTradeRef(tr.getExtTradeRef());
        this.setIsBuy(tr.getIsBuy());
        this.setTradeType(tr.getTradeType());
        this.setTradeId(tr.getTradeId());
        this.setOrderType(tr.getOrderType());
        this.setInstrumentId(tr.getInstrumentId());
        
    }
    @Override
    public BigDecimal getEffectiveRate() {
        
        BigDecimal ml = super.getEffectiveRate();
        return ml.setScale(7, RoundingMode.UP);        
    }
    
    public void ToXml(String file) throws SAXException, IOException
    {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();            
            dom = db.parse(file);
            Element TradeRootElement = dom.createElement("SpotTrade");
            dom.appendChild(TradeRootElement);
            // (String Name , String value,Element RootElement)
            
            //the trade id.
            AppendElmementInDoc("TradeID", super.getTradeId().toString(), TradeRootElement);
            //Add symbol
            AppendElmementInDoc("Symbol", super.getCcy1Code()+"/"+super.getCcy2Code(), TradeRootElement);
            //Add operation (long or short)
            AppendElmementInDoc("Operation", super.getIsBuy()?"LONG":"SHORT", TradeRootElement);
            //Add dealt rate
            AppendElmementInDoc("DealtRate", super.getDealtRate().toPlainString(), TradeRootElement);
            //Lets add the EntryPrice element.
            AppendElmementInDoc("EffectiveRate", this.getEffectiveRate().toPlainString(), TradeRootElement);
            //Order type
            AppendElmementInDoc("OrderType", super.getOrderType(), TradeRootElement);
            //Market Maker
            AppendElmementInDoc("MarketMaker", super.getExtMarketMakerCode(), TradeRootElement);
            //Date
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",Locale.US); 
            
            AppendElmementInDoc("ExecutionTime", df.format(new Date(super.getExecutionTime())), TradeRootElement);
            //OrderRef              
            AppendElmementInDoc("ExtOrderRef", super.getExtOrderRef(), TradeRootElement);
            //Source ref
            AppendElmementInDoc("ExtSourceRef", super.getExtSourceRef(), TradeRootElement);
            //Source ref
            AppendElmementInDoc("PrimeBroker", super.getPrimeBrokerRef(), TradeRootElement);
            //TimeZone
            AppendElmementInDoc("TimeZone", "0", TradeRootElement);
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(SpotXmlTrades.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     public void  TradesToFile(String path) {
        try {
            OutputFormat format = new OutputFormat(dom);
            format.setIndenting(true);


            //to generate output to console use this serializer
            XMLSerializer serializer = new XMLSerializer(System.out, format);
            serializer.serialize(dom);
            //to generate a file output use fileoutputstream instead of system.out
            serializer = new XMLSerializer(
                    new FileOutputStream(new File(path)), format);

            serializer.serialize(dom);
            dom = null;

        } catch (IOException ie) {
            ie.printStackTrace();
        }


    
}
    public Element ToXml()
    {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            dom = db.newDocument();
            Element TradeRootElement = dom.createElement("SpotTrade");
            dom.appendChild(TradeRootElement);
            // (String Name , String value,Element RootElement)
            
            //the trade id.
            AppendElmementInDoc("TradeID", getTradeId().toString(), TradeRootElement);
            //Add symbol
            AppendElmementInDoc("Symbol", getCcy1Code()+"/"+super.getCcy2Code(), TradeRootElement);
            //Add operation (long or short)
            AppendElmementInDoc("Operation", getIsBuy()?"LONG":"SHORT", TradeRootElement);
            //Add dealt rate
            AppendElmementInDoc("DealtRate", getDealtRate().toPlainString(), TradeRootElement);
            //Lets add the EntryPrice element.
            AppendElmementInDoc("EffectiveRate", this.getEffectiveRate().toPlainString(), TradeRootElement);
            //Order type
            AppendElmementInDoc("OrderType", getOrderType(), TradeRootElement);
            //Market Maker
            AppendElmementInDoc("MarketMaker", super.getExtMarketMakerCode(), TradeRootElement);
            //Date
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",Locale.US); 
            
            AppendElmementInDoc("ExecutionTime", df.format(new Date(super.getExecutionTime())), TradeRootElement);
            //OrderRef              
            AppendElmementInDoc("ExtOrderRef", super.getExtOrderRef(), TradeRootElement);
            //Source ref
            AppendElmementInDoc("ExtSourceRef", super.getExtSourceRef(), TradeRootElement);
            //Source ref
            AppendElmementInDoc("PrimeBroker", super.getPrimeBrokerRef(), TradeRootElement);
            //TimeZone
            AppendElmementInDoc("InstrumentID",getInstrumentId().name()+ ":"+ getInstrumentId().getUniqueName(), TradeRootElement);
            return TradeRootElement;
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(SpotXmlTrades.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
 /**
* A helper method (private) , which appends values and strings to the root element
*/
    private void AppendElmementInDoc(String Name, String value, Element RootElement) throws DOMException {

        //create Multiplier element and title text node and attach it to bookElement
        Element Multi = dom.createElement(Name);
        Text MultiText = dom.createTextNode(value);
        Multi.appendChild(MultiText);
        RootElement.appendChild(Multi);
        // Node appendChild = dom.appendChild(RootElement);


    }
    
}
